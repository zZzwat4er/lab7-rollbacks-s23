--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2 (Debian 15.2-1.pgdg110+1)
-- Dumped by pg_dump version 15.2

-- Started on 2023-04-30 00:15:34 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 216 (class 1259 OID 16407)
-- Name: Inventory; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public."Inventory" (
    username text NOT NULL,
    product_name text NOT NULL,
    amount integer NOT NULL,
    CONSTRAINT amount_check CHECK ((amount >= 0))
);


ALTER TABLE public."Inventory" OWNER TO admin;

--
-- TOC entry 214 (class 1259 OID 16390)
-- Name: Player; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public."Player" (
    username text NOT NULL,
    balance integer NOT NULL,
    CONSTRAINT balance_check CHECK ((balance >= 0))
);


ALTER TABLE public."Player" OWNER TO admin;

--
-- TOC entry 215 (class 1259 OID 16398)
-- Name: Shop; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public."Shop" (
    product text NOT NULL,
    in_stock integer NOT NULL,
    price integer NOT NULL,
    CONSTRAINT price_check CHECK ((price >= 0)),
    CONSTRAINT stock_check CHECK ((in_stock >= 0))
);


ALTER TABLE public."Shop" OWNER TO admin;

--
-- TOC entry 3188 (class 2606 OID 16396)
-- Name: Player Player_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public."Player"
    ADD CONSTRAINT "Player_pkey" PRIMARY KEY (username);


--
-- TOC entry 3190 (class 2606 OID 16404)
-- Name: Shop Shop_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public."Shop"
    ADD CONSTRAINT "Shop_pkey" PRIMARY KEY (product);


--
-- TOC entry 3192 (class 2606 OID 16426)
-- Name: Inventory unique_row; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public."Inventory"
    ADD CONSTRAINT unique_row UNIQUE (username, product_name);


--
-- TOC entry 3193 (class 2606 OID 16418)
-- Name: Inventory product_key; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public."Inventory"
    ADD CONSTRAINT product_key FOREIGN KEY (product_name) REFERENCES public."Shop"(product);


--
-- TOC entry 3194 (class 2606 OID 16413)
-- Name: Inventory username_key; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public."Inventory"
    ADD CONSTRAINT username_key FOREIGN KEY (username) REFERENCES public."Player"(username);


-- Completed on 2023-04-30 00:15:34 UTC

--
-- PostgreSQL database dump complete
--

INSERT INTO public."Player" (username, balance) VALUES ('Alice', 100);
INSERT INTO public."Player" (username, balance) VALUES ('Bob', 200);

INSERT INTO public."Shop" (product, in_stock, price) VALUES ('marshmello', 10, 10);

