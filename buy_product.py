import psycopg2


price_request = "SELECT price FROM public.\"Shop\" WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE public.\"Player\" SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE public.\"Shop\" SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
buy_add_to_inventory = "insert into public.\"Inventory\" (username, product_name, amount)  values (%(username)s, %(product)s, %(amount)s)  on conflict (username, product_name) do update set amount = public.\"Inventory\".amount + %(amount)s;"


def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="admin",
        password="admin",
        host="localhost",
        port=5432
    )  # TODO add your values here


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute('BEGIN')
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
                cur.execute(buy_add_to_inventory, obj)
                cur.execute('COMMIT')
            except psycopg2.errors.CheckViolation as e:
                cur.execute('ROLLBACK')
                raise e
            except Exception:
                cur.execute('ROLLBACK')
                raise

buy_product('Alice', 'marshmello', 1)